# BFI Test

Soal C
package dasar;

import java.util.Scanner;

public class SoalC {
	public static void main(String args[]) {
		
		String apapun;
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan Kata = ");
		apapun = input.nextLine();
		Integer jumlahkata = apapun.length();
		String[] splitberdasarkanspasi = apapun.split("\\s+");
		String[] arrayapapun = new String[jumlahkata];
		
		for (int i = 0; i < jumlahkata; i++) {
			if (i == 0 || i == jumlahkata-1) {
				arrayapapun[i] = apapun.charAt(i)+"";
			} else {
				String ambilkarakterberdasarkanindex = apapun.charAt(i)+"";
				if (ambilkarakterberdasarkanindex.equals(" ")) {
					arrayapapun[i] = " ";
				} else {
					arrayapapun[i] = "*";
				}
			} 
		}
		
		
		for (int i = 0; i < arrayapapun.length; i++) {
			System.out.print(arrayapapun[i]);
		}
	}
}
